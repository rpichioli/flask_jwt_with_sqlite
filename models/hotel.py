from extensions import database

class HotelModel(database.Model):
    # Table's name
    __tablename__ = 'hotels'

    # Defines columns
    hotel_id = database.Column(database.String, primary_key=True)
    name = database.Column(database.String(80))
    stars = database.Column(database.Float(precision=1))
    daily = database.Column(database.Float(precision=2))
    city = database.Column(database.String(40))

    # Constructor
    def __init__(self, hotel_id, name, stars, daily, city):
        self.hotel_id = hotel_id
        self.name = name
        self.stars = stars
        self.daily = daily
        self.city = city

    # Return a dictionary
    def json(self):
        return {
            'hotel_id': self.hotel_id,
            'name': self.name,
            'stars': self.stars,
            'daily': self.daily,
            'city': self.city
        }

    # Find Hotel by id
    # The @Classmethod is called because won't need self data
    @classmethod
    def find_hotel(cls, hotel_id):
        hotel = cls.query.filter_by(hotel_id=hotel_id).first()
        if hotel:
            return hotel
        return None

    # Save Hotel
    def save_hotel(self):
        database.session.add(self)
        database.session.commit()

    # Update Hotel
    def update_hotel(self, name, stars, daily, city):
        self.name = name
        self.stars = stars
        self.daily = daily
        self.city = city

    # Delete Hotel
    def delete_hotel(self):
        database.session.delete(self)
        database.session.commit()
