from extensions import database

class UserModel(database.Model):
    # Table's name
    __tablename__ = 'user'

    # Defines columns
    user_id = database.Column(database.Integer, primary_key=True)
    login = database.Column(database.String(40))
    password = database.Column(database.String(40))

    # Constructor
    def __init__(self, login, password):
        self.login = login
        self.password = password

    # Return a dictionary
    def json(self):
        return {
            'user_id': self.user_id,
            'login': self.login
            }

    # Find User by id
    # The @Classmethod is called because won't need self data
    @classmethod
    def find_user(cls, user_id):
        user = cls.query.filter_by(user_id=user_id).first()
        if user:
            return user
        return None

    # Find Login
    # The @Classmethod is called because won't need self data
    @classmethod
    def find_by_login(cls, login):
        user = cls.query.filter_by(login=login).first()
        if user:
            return user
        return None

    # Save User
    def save_user(self):
        database.session.add(self)
        database.session.commit()

    #  Delete User
    def delete_user(self):
        database.session.delete(self)
        database.session.commit()
