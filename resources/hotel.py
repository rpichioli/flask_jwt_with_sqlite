from flask_restx import Resource, reqparse, fields
from flask_jwt_extended import jwt_required
from models.hotel import HotelModel
from settings import api

# Swagger namespace
hotels_namespace    = api.namespace('hotels', description='Hotels operations')
hotel_namespace     = api.namespace('hotel', description='Hotel operations')

# Swagger models 
hotel_model = api.model('Hotel', {
    'hotel_id': fields.Integer(),
    'name': fields.String(),
    'stars': fields.String(),
    'daily': fields.String(),
    'city': fields.String()
})

# Request body
attributes = reqparse.RequestParser()
attributes.add_argument('name', type=str, required=True, help="The field 'name' cannot be left blank.")
attributes.add_argument('stars')
attributes.add_argument('daily')
attributes.add_argument('city')

'''
Route for /hotels>
'''
@hotels_namespace.route('')
class Hotels(Resource):
    # Return all hotels
    @hotels_namespace.marshal_list_with(hotel_model, mask='')
    def get(self):
        # It's like SELECT * FROM hotels
        return [ hotel.json() for hotel in HotelModel.query.all() ], 200 # Ok

'''
Route for /hotels/<string:hotel_id>
'''
@api.doc(params={'hotel_id': 'Hotel ID'})
@hotel_namespace.route('/<string:hotel_id>')
class Hotel(Resource):
    # Get Hotel by id
    @hotel_namespace.marshal_list_with(hotel_model, mask='')
    def get(self, hotel_id):
        # Find Hotel by id
        hotel = HotelModel.find_hotel(hotel_id)
        if hotel:
            return hotel.json(), 200 # Ok
        return {'message': 'Hotel not found.'}, 404 # Not found

    # Post Hotel
    # Necessary JWT
    @jwt_required
    @hotel_namespace.marshal_list_with(hotel_model, mask='') # Output in swagger
    @hotel_namespace.expect(hotel_model, validate=False) # Model in swagger
    def post(self, hotel_id):
        # Checks if it already exists
        if HotelModel.find_hotel(hotel_id):
            return {"message": "Hotel id '{}' already exists.".format(hotel_id)}, 400 # Bad Request

        # Request Body data
        data = attributes.parse_args()
        # Instantiate a new hotel
        hotel = HotelModel(hotel_id, **data)

        try:
            # Save new Hotel
            hotel.save_hotel()
        except:
            # If theres errors
            return {"message": "An error ocurred trying to create hotel."}, 500 # Internal Server Error

        # If all goes well
        return hotel.json(), 201 # Created

    # Put Hotel
    # Necessary JWT
    @jwt_required
    @hotel_namespace.marshal_list_with(hotel_model, mask='')
    def put(self, hotel_id):
        # Request Body data
        data = attributes.parse_args()
        # Instantiate a new hotel
        hotel = HotelModel(hotel_id, **data)
        # Find Hotel by id
        hotel_found = HotelModel.find_hotel(hotel_id)

        if hotel_found:
            # Update Hotel
            hotel_found.update_hotel(**data)
            # Save Hotel updated in  DB
            hotel_found.save_hotel()
            return hotel_found.json(), 200 # Ok
        
        # If hotel not found, create a new
        hotel.save_hotel()
        # If all goes well
        return hotel.json(), 201 # Created

    # Delete Hotel
    # Necessary JWT
    @jwt_required
    def delete(self, hotel_id):
        # Find Hotel by id
        hotel = HotelModel.find_hotel(hotel_id)
        if hotel:
            # Delete Hotel
            hotel.delete_hotel()
            return {'message': 'Hotel deleted.'}, 200 # Ok
        return {'message': 'Hotel not found.'}, 404 # Not found
