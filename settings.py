import os
from flask import Flask, jsonify
from flask_restx import Api, Resource, fields

# ---------------------------
# -- Environment variables --
# ---------------------------
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI') # String connection
JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY') # JWT Secret

# Launches the app
app = Flask(__name__)

# Config app with settings.py
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # Don't show DB's errors 
app.config['JWT_BLACKLIST_ENABLED'] = True # JWT's list users loggeddea
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['JWT_SECRET_KEY'] = JWT_SECRET_KEY

# Config Swagger authorizations
authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

# Lauches the api with the app and config Swagger
api = Api(
    app,
    version='1.0',
    title='Flask API',
    description='A simple Flask API with JWT, SQLite and Swagger',
    authorizations=authorizations,
    security='apikey'
)